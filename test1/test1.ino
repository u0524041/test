#include <Wire.h>
#define KS103ADD 0x74    // Address of the 0xe8  选择串口地址，共有20个
word distance=0;
const int trig = 5;
const int echo = 6;
const int inter_time = 10;
int time = 0;

void setup() {
  Serial.begin(9600);
  pinMode (trig, OUTPUT);
  pinMode (echo, INPUT);
  Wire.begin();     //链接线路                          
  delay(100);                         // Waits to make sure everything is powered up before sending or receiving data

  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));     
  Wire.write(0x71);   // 发送降噪指令   
  Wire.endTransmission();     
  delay(10);         
}

void loop() {
  float duration, distance;
  digitalWrite(trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trig, LOW);
  duration = pulseIn (echo, HIGH);
  distance = (duration/2)/29;
  Serial.print("Data:");
  Serial.print (time/1000);
  Serial.print(", d = ");
  Serial.print(distance);
  Serial.println(" cm");
  time = time + inter_time;
  delay(100);
  KS103_read();
  Serial.println(distance);
  delay(200);                                     // Wait before looping
}

 word KS103_read(){
      Wire.beginTransmission(KS103ADD);
      Wire.write(byte(0x02));     
      Wire.write(0xb0);     //量程设置为5m 不带温度补偿
      Wire.endTransmission();     
      delay(1);               
      Wire.beginTransmission(KS103ADD);
      Wire.write(byte(0x02));   
      Wire.endTransmission();   
      Wire.requestFrom(KS103ADD, 2);  
      if(2 <= Wire.available())   
      {
        distance = Wire.read();
        distance =  distance << 8;   
        distance |= Wire.read();
      }
    }

