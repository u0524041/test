#include <HCSR04.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "Adafruit_VL53L0X.h"
#define KS103ADD  0x74
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
SoftwareSerial mySerial(1, 0);

int brakedistance, geardistance = 0;
char gearstalls;
const int gearpot = 3;
const int HandBrake = 4;
const int LeftPin = 6;
const int RightPin = 5;
const int Baba = 7;
const int MainBeam = 8;
const int SmallBeam = 9;
const int INT = 10;
const int Low = 11;
const int High = 12;
const int Wash = 13;

int State_I, State_Low, State_H, State_L, State_R, State_M, State_S, State_W, State_HB, State_B = 0;

void setup()
{
  Wire.begin();
  lox.begin();
  Serial.begin(9600);
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.write(0x71);   // 发送降噪指令
  Wire.endTransmission();
  mySerial.begin(9600);
  pinMode(1 , OUTPUT);
  pinMode(0 , INPUT);
  pinMode(INT , INPUT);
  pinMode(Low , INPUT);
  pinMode(High , INPUT);
  pinMode(Wash , INPUT);
  pinMode(LeftPin , INPUT);
  pinMode(RightPin , INPUT);
  pinMode(MainBeam , INPUT);
  pinMode(SmallBeam , INPUT);
  pinMode(HandBrake , INPUT);
  pinMode(Baba , INPUT);
  delay(1);

}

void loop()
{
  KS103_read();
  VL53L0X_RangingMeasurementData_t measure;
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
  Serial.print("{\"ThrottlePedal\":");
  Serial.print(throttlepercent(measure.RangeMilliMeter));
  //Serial.print(":");
  //Serial.print(measure.RangeMilliMeter);
  Serial.print(",\"BrakePedal\":");
  Serial.print(brakepercent(brakedistance));
  //Serial.print(":");
  //Serial.print(brakedistance);
  gearrange();
  TurnSignal();
  Wipe();
  HandBrakef();
  Babaf();
  Serial.println("}");
  delay(1);
}

word KS103_read() {
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.write(0xb0);     //量程设置为5m 不带温度补偿
  Wire.endTransmission();
  delay(1);
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.endTransmission();
  Wire.requestFrom(KS103ADD, 4);
  if (4 <= Wire.available())
  {
    brakedistance = Wire.read();
    brakedistance =  brakedistance << 8;
    brakedistance |= Wire.read();
  }
}

inline long throttlepercent(int x) { //throttle disstance change to persent function
  int tmax = 125;//sensor to throttle max value
  int tmix = 100;//sensor to throttle mix value
  int throttlerange = (tmax - tmix); //sensor to throttle - mix
  int throttlepers = 0;
  if (x <= tmax) {
    throttlepers = (100 - (((x - tmix) * 100) / throttlerange)); //distance change to persent
    if (throttlepers <= 100) { //until full throttle
      return throttlepers;
    }
    else {
      return 100;
    }
  } else {
    return 0;
  }
}
inline int brakepercent(int y) { //brake disstance change to persent function
  int bmax = 140;//sensor to brake max value
  int bmix = 30;//sensor to brake mix value
  int brakerange = (bmax - bmix); //sensor to brake - mix
  if (y <= bmax) {
    y = (100 - (((y - bmix) * 100) / brakerange)); //distance change to persent
    if (y <= 100) { //until full brake
      return y;
    }
    else {
      return 100;
    }
  } else {
    return 0;
  }
}
inline  char gearrange() {
  Serial.print(",\"Gear\":");
  int gearvalue = analogRead(gearpot);
  Serial.print(gearvalue);
  /*if( g >= 80 && g <= 150 ){
    Serial.print("\"P\"");}
    else if( g >= 151 && g <= 175 ){
      Serial.print("\"R\"");}
    else if( g >= 176 && g <= 187 ){
      Serial.print("\"N\"");}
    else if( g >= 188 && g <= 300 ){
      Serial.print("\"D\"");}
    else{
    Serial.print("\"Null\"");
  */

}

inline int Wipe() {
  State_I = digitalRead(INT);
  State_Low = digitalRead(Low);
  State_H = digitalRead(High);
  State_W = digitalRead(Wash);
  Serial.print(",\"Wipers\":");
  if (State_I == HIGH) {
    Serial.print(1);
  }
  else if (State_Low == HIGH) {
    Serial.print(10);
  }
  else if (State_H == HIGH) {
    Serial.print(100);
  }
  else if (State_W == HIGH) {
    Serial.print(1000);
  }
  else {
    Serial.print(0);
  }
}

inline int TurnSignal() {
  State_L = digitalRead(LeftPin);
  State_R = digitalRead(RightPin);
  State_M = digitalRead(MainBeam);
  State_S = digitalRead(SmallBeam);
  Serial.print(",\"TurnSignal\":");
  if (State_L == HIGH) {
    Serial.print("\"L\"");
  }
  else if (State_R == HIGH) {
    Serial.print("\"R\"");
  }
  else {
    Serial.print("\"N\"");
  }

  Serial.print(",\"Headlights\":");
  if (State_M == HIGH) {
    Serial.print(10);
  }
  else if (State_S == HIGH) {
    Serial.print(1);
  }
  else {
    Serial.print(0);
  }
  delay(10);
}

inline int HandBrakef() {
  State_HB = digitalRead(HandBrake);
  Serial.print(",\"HandBrake\":");
  if (State_HB == HIGH) {
    Serial.print(1);
  } else {
    Serial.print(0);
  }
}


inline int Babaf() {
  State_B = digitalRead(Baba);
  Serial.print(",\"Baba\":");
  if (State_B == HIGH) {
    Serial.print(1);
  } else {
    Serial.print(0);
  }
}

