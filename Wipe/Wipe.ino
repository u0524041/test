#include <SoftwareSerial.h>
const int INT = 10;
const int Low = 11;
const int High = 12;
const int Wash = 13;
int State_I, State_Low, State_H = 0;
int State_W = 0;

void setup() {
  Serial.begin(9600);
  pinMode(INT , INPUT);
  pinMode(Low , INPUT);
  pinMode(High , INPUT);
  pinMode(Wash , INPUT);
}

void loop(){
  State_I = digitalRead(INT);
  State_Low = digitalRead(Low);
  State_H = digitalRead(High);
  State_W = digitalRead(Wash);
  if(State_I == HIGH){
    Serial.println(1);
  }
  else if(State_Low == HIGH){
    Serial.println(10);
  }
  else if(State_H == HIGH){
    Serial.println(100);
  }
  else if(State_W == HIGH){
    Serial.println(1000);
  }
  else{
    Serial.println(0);
  }
