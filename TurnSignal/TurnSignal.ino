#include <SoftwareSerial.h>
const int LeftPin = 2;
const int RightPin = 3;
const int MainBeam = 8;
const int SmallBeam = 9;
int State_L = 0;
int State_R = 0;
int State_M = 0;
int State_S = 0;
void setup() {
  Serial.begin(9600);
  pinMode(LeftPin , INPUT);
  pinMode(RightPin , INPUT);
  pinMode(MainBeam , INPUT);
  pinMode(SmallBeam , INPUT);
}

void loop() {
  State_L = digitalRead(LeftPin);
  State_R = digitalRead(RightPin);
  State_M = digitalRead(MainBeam);
  State_S = digitalRead(SmallBeam);
  if(State_L == HIGH){
    Serial.println("L");
  }
    else if(State_R == HIGH){
    Serial.println("R");
  }
  else{
    Serial.println("N");
  }
  if(State_M == HIGH){
    Serial.println(10);
  }
    else if(State_S == HIGH){
    Serial.println(1);
  }
  else{
    Serial.println(0);
  }

  delay(100);

}
