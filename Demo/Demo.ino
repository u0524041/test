#include <HCSR04.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(1,0);
UltraSonicDistanceSensor throttle(5, 6);  // throttle sensor that uses digital pins 5 and 6.
UltraSonicDistanceSensor brake(2, 3);  // brake sensor that uses digital pins 2 and 3.
int throttledistance,brakedistance =0;
void setup () {
    Serial.begin(9600);  // We initialize serial connection so that we could print values from sensor.
    mySerial.begin(9600);
    pinMode(1,OUTPUT);
    pinMode(0,INPUT);
    delay(1);
}

void loop () {
    // Every 500 miliseconds, do a measurement using the sensor and print the distance in centimeters.
    throttledistance = (throttle.measureDistanceCm()*10);
    brakedistance = (brake.measureDistanceCm()*10);
    Serial.print(throttledistance);
    Serial.print(',');
    Serial.println(brakedistance);
    delay(10);
}

    inline long thrdisfix(int x){  //throttle disstance change to persent
      int tmax = 47;//sensor to throttle max value
      int tmix = 20;//sensor to throttle mix value
      int throttlerange =(tmax-tmix);//sensor to throttle - mix
      if (x <= tmax){

        x = (100-(((x-tmix)*100)/throttlerange));//distance change to persent
        if (x<=100){//until full throttle
        return x;
        }
        else{
          return 100;
        }

      }else{
        return 0;
      }
    }

    inline long brkdisfix(int y){  //brake disstance change to persent
      int bmax = 45;//sensor to brake max value
      int bmix = 20;//sensor to brake mix value
      int brarange =(bmax-bmix);//sensor to brake - mix
      if (y <= 110){

        y =(100-(((y-bmix)*100)/brakerange));//distance change to persent
        if (y<=100){ //until full brake
          return y;
        }
        else{
          return 100;
        }

      }else{
        return 0;
      }
    }
